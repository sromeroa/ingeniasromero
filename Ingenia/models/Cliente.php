<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cliente".
 *
 * @property int $idCliente
 * @property string $nombre
 * @property string $apellidos
 * @property string $email
 * @property string $telefono
 * @property string $tarjetaCredito
 */
class Cliente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cliente';
    }
    
   

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellidos', 'email', 'telefono', 'tarjetaCredito'], 'required'],
            [['nombre'], 'string', 'max' => 200],
            [['apellidos'], 'string', 'max' => 300],
            [['email'], 'email'],
            [['telefono'], 'string', 'max' => 10],
            [['tarjetaCredito'], 'string', 'max' => 16],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        
        return [
            'idCliente' => 'Id Cliente',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'email' => 'Email',
            'telefono' => 'Telefono',
            'tarjetaCredito' => 'Tarjeta Credito',
          //  'estatus' => 'Estatus',
        ];
    }
    
    
}

